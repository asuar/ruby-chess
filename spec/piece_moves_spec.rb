# frozen_string_literal: true

require './lib/movement/piece_moves.rb'
require './lib/coordinate.rb'

describe PieceMoves do
  let(:starting_position) { Coordinate.new(starting_x, starting_y) }
  let(:starting_x) { @x }
  let(:starting_y) { @y }

  describe '.generate_moves' do
    context 'when piece is a pawn' do
      it 'returns all generate moves from its current position' do
        @x = 1
        @y = 2
        expect(PieceMoves.generate_moves(starting_position, 1)).to eq([Coordinate.new(1, 3), Coordinate.new(2, 3), Coordinate.new(0, 3)])
      end

      context 'when pawn has not moved before' do
        it 'can move additional spaces' do
          @x = 0
          @y = 1
          expect(PieceMoves.generate_moves(starting_position, 1)).to eq([Coordinate.new(0, 2), Coordinate.new(0, 3), Coordinate.new(1, 2), Coordinate.new(-1, 2)])
        end
      end

      context 'when pawn is black' do
        it 'moves down instead of up' do
          @x = 0
          @y = 6
          expect(PieceMoves.generate_moves(starting_position, -1)).to eq([Coordinate.new(0, 5), Coordinate.new(0, 4), Coordinate.new(1, 5), Coordinate.new(-1, 5)])
        end
      end
    end

    context 'when piece is a knight' do
      it 'returns all generate moves from its current position' do
        @x = 0
        @y = 0
        expect(PieceMoves.generate_moves(starting_position, 2)).to eq([Coordinate.new(1, 2), Coordinate.new(2, 1), Coordinate.new(2, -1),
                                                                          Coordinate.new(1, -2), Coordinate.new(-1, -2), Coordinate.new(-2, -1),
                                                                          Coordinate.new(-2, 1), Coordinate.new(-1, 2)])
      end
    end

    context 'when piece is a bishop' do
      it 'returns all generate moves from its current position' do
        @x = 0
        @y = 0
        expect(PieceMoves.generate_moves(starting_position, 3)).to eq([Coordinate.new(1, 1), Coordinate.new(2, 2), Coordinate.new(3, 3), Coordinate.new(4, 4),
                                                                          Coordinate.new(5, 5), Coordinate.new(6, 6), Coordinate.new(7, 7),
                                                                          Coordinate.new(-1, 1), Coordinate.new(-2, 2), Coordinate.new(-3, 3), Coordinate.new(-4, 4),
                                                                          Coordinate.new(-5, 5), Coordinate.new(-6, 6), Coordinate.new(-7, 7),
                                                                          Coordinate.new(1, -1), Coordinate.new(2, -2), Coordinate.new(3, -3), Coordinate.new(4, -4),
                                                                          Coordinate.new(5, -5), Coordinate.new(6, -6), Coordinate.new(7, -7),
                                                                          Coordinate.new(-1, -1), Coordinate.new(-2, -2), Coordinate.new(-3, -3), Coordinate.new(-4, -4),
                                                                          Coordinate.new(-5, -5), Coordinate.new(-6, -6), Coordinate.new(-7, -7)])
      end
    end

    context 'when piece is a rook' do
      it 'returns all generate moves from its current position' do
        @x = 0
        @y = 0
        expect(PieceMoves.generate_moves(starting_position, 4)).to eq([Coordinate.new(0, 1), Coordinate.new(0, 2), Coordinate.new(0, 3), Coordinate.new(0, 4),
                                                                          Coordinate.new(0, 5), Coordinate.new(0, 6), Coordinate.new(0, 7),
                                                                          Coordinate.new(0, -1), Coordinate.new(0, -2), Coordinate.new(0, -3), Coordinate.new(0, -4),
                                                                          Coordinate.new(0, -5), Coordinate.new(0, -6), Coordinate.new(0, -7),
                                                                          Coordinate.new(1, 0), Coordinate.new(2, 0), Coordinate.new(3, 0), Coordinate.new(4, 0),
                                                                          Coordinate.new(5, 0), Coordinate.new(6, 0), Coordinate.new(7, 0),
                                                                          Coordinate.new(-1, 0), Coordinate.new(-2, 0), Coordinate.new(-3, 0), Coordinate.new(-4, 0),
                                                                          Coordinate.new(-5, 0), Coordinate.new(-6, 0), Coordinate.new(-7, 0)])
      end
    end

    context 'when piece is a queen' do
      it 'returns all generate moves from its current position' do
        @x = 0
        @y = 0
        expect(PieceMoves.generate_moves(starting_position, 5)).to eq([Coordinate.new(0, 1), Coordinate.new(0, 2), Coordinate.new(0, 3), Coordinate.new(0, 4),
                                                                          Coordinate.new(0, 5), Coordinate.new(0, 6), Coordinate.new(0, 7),
                                                                          Coordinate.new(0, -1), Coordinate.new(0, -2), Coordinate.new(0, -3), Coordinate.new(0, -4),
                                                                          Coordinate.new(0, -5), Coordinate.new(0, -6), Coordinate.new(0, -7),
                                                                          Coordinate.new(1, 0), Coordinate.new(2, 0), Coordinate.new(3, 0), Coordinate.new(4, 0),
                                                                          Coordinate.new(5, 0), Coordinate.new(6, 0), Coordinate.new(7, 0),
                                                                          Coordinate.new(-1, 0), Coordinate.new(-2, 0), Coordinate.new(-3, 0), Coordinate.new(-4, 0),
                                                                          Coordinate.new(-5, 0), Coordinate.new(-6, 0), Coordinate.new(-7, 0),
                                                                          Coordinate.new(1, 1), Coordinate.new(2, 2), Coordinate.new(3, 3), Coordinate.new(4, 4),
                                                                          Coordinate.new(5, 5), Coordinate.new(6, 6), Coordinate.new(7, 7),
                                                                          Coordinate.new(-1, 1), Coordinate.new(-2, 2), Coordinate.new(-3, 3), Coordinate.new(-4, 4),
                                                                          Coordinate.new(-5, 5), Coordinate.new(-6, 6), Coordinate.new(-7, 7),
                                                                          Coordinate.new(1, -1), Coordinate.new(2, -2), Coordinate.new(3, -3), Coordinate.new(4, -4),
                                                                          Coordinate.new(5, -5), Coordinate.new(6, -6), Coordinate.new(7, -7),
                                                                          Coordinate.new(-1, -1), Coordinate.new(-2, -2), Coordinate.new(-3, -3), Coordinate.new(-4, -4),
                                                                          Coordinate.new(-5, -5), Coordinate.new(-6, -6), Coordinate.new(-7, -7)])
      end
    end

    context 'when piece is a king' do
      it 'returns all generate moves from its current position' do
        @x = 0
        @y = 0
        expect(PieceMoves.generate_moves(starting_position, 6)).to eq([Coordinate.new(0, 1), Coordinate.new(0, -1), Coordinate.new(1, 0), Coordinate.new(-1, 0),
                                                                          Coordinate.new(1, 1), Coordinate.new(-1, 1), Coordinate.new(1, -1), Coordinate.new(-1, -1)])
      end
    end
  end
end
