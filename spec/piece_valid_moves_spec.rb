# frozen_string_literal: true

require './lib/movement/piece_valid_moves.rb'

describe PieceValidMoves do
  subject(:validator) { PieceValidMoves.new(board) }
  let(:piece_location) { Coordinate.new(x, y) }
  let(:x) { @x }
  let(:y) { @y }

  let(:starting_position) { Coordinate.new(starting_x, starting_y) }
  let(:starting_x) { @x }
  let(:starting_y) { @y }
  let(:en_passant) {nil}
  let(:castling_allowed) { "KQkq" }

  describe '.generate_pseudo_legal_moves' do
    context 'when piece is blocked in by allied pieces' do
      let(:board) do
        board = (Array.new(8) { Array.new(8) { 0 } })
        #     1 -- Pawn
        #     2 -- Knight
        #     3 -- Bishop
        #     4 -- Rook
        #     5 -- Queen
        #     6 -- King
        #     negative for black
        # setup black pieces
        board[7] = [-4, -2, -3, -5, -6, -3, -2, -4]
        board[6] = [-1, -1, -1, -1, -1, -1, -1, -1]
        # setup white pieces
        board[1] = [1, 1, 1, 1, 1, 1, 1, 1]
        board[0] = [4, 2, 3, 5, 6, 3, 2, 4]
        @b = board
      end
      let(:castling_allowed) { "KQkq" }
      context 'and not a knight' do
        it 'has no valid moves' do
          @x = 0
          @y = 0
          expect(validator.generate_pseudo_legal_moves(piece_location,en_passant, castling_allowed)).to eq([])
        end
      end

      context 'and is a knight' do
        it 'has valid moves' do
          @x = 1
          @y = 0
          expect(validator.generate_pseudo_legal_moves(piece_location,en_passant, castling_allowed)).to eq([Coordinate.new(2, 2), Coordinate.new(0, 2)])
        end
      end
    end

    context 'when piece is not blocked in by allied pieces' do
      let(:board) do
        board = (Array.new(8) { Array.new(8) { 0 } })
        #     1 -- Pawn
        #     2 -- Knight
        #     3 -- Bishop
        #     4 -- Rook
        #     5 -- Queen
        #     6 -- King
        #     negative for black
        # setup black pieces
        board[7] = [-4, -2, -3, -5, -6, -3, -2, -4]
        board[6] = [-1, -1, -1, -1, -1, -1, -1, -1]
        # setup white pieces
        board[2] = [0, 0, 0, -1, 0, 0, 0, 0]
        board[1] = [1, 4, 1, 1, 1, 1, 1, 1]
        board[0] = [4, 2, 3, 5, 6, 3, 2, 4]
        @b = board
      end
      let(:castling_allowed) { "KQkq" }
      context 'and is not a pawn' do
        it 'has valid moves' do
          @x = 1
          @y = 1
          expect(validator.generate_pseudo_legal_moves(piece_location,en_passant, castling_allowed)).to eq([Coordinate.new(1, 2), Coordinate.new(1, 3), Coordinate.new(1, 4), Coordinate.new(1, 5), Coordinate.new(1, 6)])
        end
      end

      context 'and is a pawn' do
        context 'and has enemy piece diagonal to it' do
          it 'can capture it' do
            @x = 2
            @y = 1
            expect(validator.generate_pseudo_legal_moves(piece_location,en_passant, castling_allowed)).to eq([Coordinate.new(2, 2), Coordinate.new(2, 3), Coordinate.new(3, 2)])
          end
        end

        context 'and doesnt have an enemy piece diagonal to it' do
          it 'cannot move diagonally' do
            @x = 6
            @y = 1
            expect(validator.generate_pseudo_legal_moves(piece_location,en_passant, castling_allowed)).to eq([Coordinate.new(6, 2), Coordinate.new(6, 3)])
          end
        end
      end
    end

    context 'when piece is moving' do
      let(:board) do
        board = (Array.new(8) { Array.new(8) { 0 } })
        #     1 -- Pawn
        #     2 -- Knight
        #     3 -- Bishop
        #     4 -- Rook
        #     5 -- Queen
        #     6 -- King
        #     negative for black
        # setup black pieces
        board[7] = [-4, -2, -3, -5, -6, -3, -2, -4]
        board[6] = [0, -1, -1, -1, -1, -1, -1, -1]
        # setup white pieces
        board[2] = [1, 0, 0, 0, 0, 0, 0, 0]
        board[1] = [0, 1, 0, 1, 1, 1, 1, 1]
        board[0] = [4, 2, 3, 5, 6, 3, 2, 4]
        @b = board
      end
      let(:castling_allowed) { "KQkq" }
      context 'into a space whose path is not blocked' do
        it 'can move to it' do
          @x = 0
          @y = 2
          expect(validator.generate_pseudo_legal_moves(starting_position, en_passant, castling_allowed)).to eq([Coordinate.new(0, 3)])
        end
      end

      context 'into a space whose path is blocked' do
        context 'by a same color piece' do
          it 'cant move to the blocked space or any after it' do
            @x = 0
            @y = 0
            expect(validator.generate_pseudo_legal_moves(starting_position, en_passant, castling_allowed)).to eq([Coordinate.new(0, 1)])
          end
        end

        context 'by a different color piece' do
          it 'can move to the blocked space but not any after it' do
            @x = 0
            @y = 7
            expect(validator.generate_pseudo_legal_moves(starting_position, en_passant, castling_allowed)).to eq([Coordinate.new(0, 6),
                                                                                              Coordinate.new(0, 5),
                                                                                              Coordinate.new(0, 4),
                                                                                              Coordinate.new(0, 3),
                                                                                              Coordinate.new(0, 2)])
          end
        end
      end
      it 'can move its full range' do
        @x = 3
        @y = 0
        expect(validator.generate_pseudo_legal_moves(starting_position, en_passant, castling_allowed)).to eq([Coordinate.new(2, 1),
                                                                                          Coordinate.new(1, 2),
                                                                                          Coordinate.new(0, 3)])
      end
    end
  end
end
