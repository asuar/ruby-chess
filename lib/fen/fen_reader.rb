# frozen_string_literal: true

module FenReader
  require_relative '../coordinate.rb'
  require_relative '../board_state.rb'
  require_relative '../utils.rb'

  @@WHITE_LETTER = 'w'
  @@BLACK_LETTER = 'b'

  def self.read_fen_string(fen_string)
    fen_string_parts = split_fen_string(fen_string)
    board = fen_string_to_board(fen_string_parts[0])
    player_to_move = fen_string_to_player(fen_string_parts[1])
    castling = fen_string_parts[2]
    en_passant = fen_string_to_en_passant(fen_string_parts[3])
    capture_clock = fen_string_parts[4].to_i
    fullmove_clock = fen_string_parts[5].to_i

    BoardState.new(board, player_to_move, castling, en_passant, capture_clock, fullmove_clock)
  end

  private

  def self.split_fen_string(fen_string)
    fen_split = fen_string.split(' ')
  end

  def self.fen_string_to_board(fen_string)
    board_strings = fen_string.split('/')
    result = []

    board_strings.reverse.each do |row|
      row_pieces = []
      row.each_char do |position|
        if is_number?(position)
          position.to_i.times do
            row_pieces << 0
          end
        else
          row_pieces << get_piece_from_string(position)
        end
      end
      result << row_pieces
    end
    result
  end

  def self.is_number?(string)
    true if Float(string)
  rescue StandardError
    false
  end

  def self.fen_string_to_player(fen_string)
    fen_string == @@WHITE_LETTER ? Utils::WHITE : Utils::BLACK
  end

  def self.fen_string_to_en_passant(fen_string)
    return nil if fen_string == '-'

    en_passant = Coordinate.new(fen_string.bytes.to_a[0] - 97, fen_string[1].to_i - 1)
  end

  def self.get_piece_from_string(string)
    case string
    when 'r'
      Utils::BLACK_ROOK
    when 'n'
      Utils::BLACK_KNIGHT
    when 'b'
      Utils::BLACK_BISHOP
    when 'q'
      Utils::BLACK_QUEEN
    when 'k'
      Utils::BLACK_KING
    when 'p'
      Utils::BLACK_PAWN
    when 'R'
      Utils::WHITE_ROOK
    when 'N'
      Utils::WHITE_KNIGHT
    when 'B'
      Utils::WHITE_BISHOP
    when 'Q'
      Utils::WHITE_QUEEN
    when 'K'
      Utils::WHITE_KING
    when 'P'
      Utils::WHITE_PAWN
    end
  end
end
