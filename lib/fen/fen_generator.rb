module FenGenerator
  require_relative '../board_state.rb'
  require_relative '../utils.rb'

  @@WHITE_LETTER = 'w'
  @@BLACK_LETTER = 'b'

  def self.generate_fen_string(boardstate)
    result = convert_board_to_fen(boardstate.board) + ' '
    result << convert_player_to_fen(boardstate.active_color) + ' '
    result << boardstate.castling + ' '
    result << convert_en_passant_to_fen(boardstate.en_passant_target) + ' '
    result << boardstate.halfmove_count.to_s + ' '
    result << boardstate.fullmove_count.to_s
  end

  private

  def self.convert_board_to_fen(board)
    result = ''
    board.reverse.each_with_index do |row, index_row|
      sum = 0
      row.each_with_index do |position, _index_column|
        if position == 0
          sum += 1
        else
          if sum > 0
            result << sum.to_s
            sum = 0
            end
          result << get_piece_char(position)
        end
      end
      result << sum.to_s if sum > 0
      result << '/' if index_row != (board.size - 1)
    end
    result
  end

  def self.convert_player_to_fen(player)
    player == Utils::WHITE ? @@WHITE_LETTER : @@BLACK_LETTER
  end

  def self.get_piece_char(position)
    case position
    when Utils::BLACK_PAWN
      'p'
    when Utils::BLACK_KNIGHT
      'n'
    when Utils::BLACK_BISHOP
      'b'
    when Utils::BLACK_ROOK
      'r'
    when Utils::BLACK_QUEEN
      'q'
    when Utils::BLACK_KING
      'k'
    when Utils::WHITE_PAWN
      'P'
    when Utils::WHITE_KNIGHT
      'N'
    when Utils::WHITE_BISHOP
      'B'
    when Utils::WHITE_ROOK
      'R'
    when Utils::WHITE_QUEEN
      'Q'
    when Utils::WHITE_KING
      'K'
    end
  end

  def self.convert_en_passant_to_fen(en_passant_target)
    return '-' if en_passant_target.nil?

    en_passant = ''
    en_passant += (en_passant_target.x.to_i + 97).chr + (en_passant_target.y.to_i + 1).to_s
  end
end
