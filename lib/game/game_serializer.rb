# frozen_string_literal: true

GameState = Struct.new(:white_player,:black_player,:position_fen_string)

class GameSerializer
  require 'json'

  def initialize; end

  public

  def save_game(gamestate)
    Dir.mkdir('saves') unless Dir.exist?('saves/')
    filename = create_savefile_name
    file = File.new(filename, 'w')
    file.puts to_json(gamestate)
    file.close
  end

  def load_game
    if Dir.exist?('saves/')
      from_json(read_savefile)
    else
      puts 'No save files exist!'
    end
end

  private

  def to_json(gamestate)
    JSON.dump ({
      white_player: gamestate.white_player,
      black_player: gamestate.black_player,
      position_fen_string: gamestate.position_fen_string
    })
  end

  def from_json(string)
    data = JSON.load string
    GameState.new(data['white_player'],data['black_player'],data['position_fen_string'])
  end

  
  def choose_save_file
    files = []
    Dir.foreach('saves/') do |file|
      next if (file == '.') || (file == '..')

      files << file[0..-6].downcase
    end

    file_name = ''
    until files.include? file_name
      puts 'Save files:'
      puts files
      puts 'Choose a save by name:'
      file_name = gets.chomp
    end
    file_name
  end

  def create_savefile_name
    if @loaded_filename.nil?

      user_input = get_filename_from_user
      filename = "saves/#{user_input}.json"

      counter = 1
      while File.exist?(filename)
        filename = "saves/#{user_input}#{counter}.json"
        counter += 1
      end
    else
      filename = "saves/#{@loaded_filename}.json"
      end
    filename
  end

  def read_savefile
    save_file_name = choose_save_file
    save_file = File.new('saves/' + save_file_name.capitalize + '.json', 'r')
    contents = save_file.read
    save_file.close
    @loaded_filename = save_file_name
    contents
  end

  def get_filename_from_user
    puts "Enter a name for the save file:"
    gets.chomp
  end
end
