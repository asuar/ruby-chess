class Coordinate
    attr_accessor :x, :y

    def initialize(x,y)
        @x = x
        @y = y
    end

    public

    def ==(o)
        @x == o.x && @y == o.y
    end

    def to_s
        "#{@x},#{@y}"
    end
end