# frozen_string_literal: true

class Perft
  require_relative 'board.rb'
  require_relative 'movement/piece_valid_moves.rb'
  require_relative 'coordinate.rb'
  require_relative 'utils.rb'

  @@PIECES = [['bishop', 'b'], ['rook', 'r'], ['knight', 'n'], ['queen', 'q']]

  def get_perft(depth, fen = nil)
    board = Board.new
    board.read_fen_string(fen) unless fen.nil?
    sum, divide_counts = perft(depth, board)
    [sum, divide_counts]
  end

  def get_divide(depth, fen = nil)
    sum, divide_counts  = get_perft(depth, fen)
    print_divide(sum, divide_counts)
    [sum, divide_counts]
  end

  private

  def perft(depth, board)
    return [1, []] if depth == 0
    legal_moves = get_legal_moves(board)
    #change to allow this:
    #return [legal_moves.length, []] if depth == 1
    
    total_move_count, divide_counts = get_move_info_from_legal_moves(board, depth, legal_moves)
    [total_move_count, divide_counts]
  end

  def convert_to_move_text(move, piece = "")
    piece_to_move = move[0]
    destination = move[1]
    (piece_to_move.x.to_i + 65).chr + (piece_to_move.y.to_i + 1).to_s + (destination.x.to_i + 65).chr + (destination.y.to_i + 1).to_s + piece
  end

  def get_legal_moves(board)
    total_locations = board.get_color_pieces(board.turn_to_move)
    movable_locations = []
    legal_move_with_promotion = []
  
    total_locations.each_with_index do |location, index|
        current_position = Coordinate.new(location[0][0], location[0][1])      
        piece_type, piece_color = board.get_piece_info(current_position)

        legal_moves = board.get_piece_legal_moves(current_position)
        legal_move_with_promotion += legal_moves.map{|move| 
          promotion = (piece_type == Utils::WHITE_PAWN && move.y == 7) || (piece_type == Utils::BLACK_PAWN && move.y == 0) ? 1 : 0
          [current_position, move, promotion]
        }
        movable_locations << current_position if legal_moves.size > 0
    end


    legal_move_with_promotion
  end
 
  def get_move_info_from_legal_moves(board, depth, legal_moves)
    total_move_count = 0
    divide_counts = []
    legal_moves.each do |move|


      move_result = move[2] == 1 ? get_promotion_moves_info(board, move, depth) : get_move_info(board, move, depth) 
      next if move_result.nil? 
      divide_counts += move_result[1]
      total_move_count += move_result[0]


    end

    [total_move_count, divide_counts]
  end

  def get_promotion_moves_info(board, move, depth)
    move_count = 0
    divide_counts = []

    @@PIECES.each do |piece|
      move_result = board.move_piece(move[0], move[1])
      next unless move_result
      board.promote_pawn(piece[0])
      sum, move_divide_counts  = perft(depth - 1, board)
      divide_counts << ["#{convert_to_move_text(move)}#{piece[1]}", sum]
      move_count += sum
      board.undo_move
    end

    [move_count, divide_counts]
  end

  def get_move_info(board, move, depth)
    move_count = 0
    divide_counts = []
    move_result = board.move_piece(move[0], move[1])
    return nil unless move_result
    sum, move_divide_counts  = perft(depth - 1, board)
    divide_counts << ["#{convert_to_move_text(move)}", sum]
    move_count += sum
    board.undo_move

    [move_count, divide_counts]
  end

  def print_divide(divide_total, divide_counts)
    puts "#{divide_counts.map{|result| result[0] + ': ' + result[1].to_s}.join("\n")}"
    puts "Total: #{divide_total}"
  end
end
