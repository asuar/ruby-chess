# frozen_string_literal: true

class Board
  require_relative 'movement/piece_valid_moves.rb'
  require_relative 'coordinate.rb'
  require_relative 'fen/fen_generator.rb'
  require_relative 'fen/fen_reader.rb'
  require_relative 'utils.rb'

  attr_reader :board, :fullmove_count, :turn_to_move, :game_over_reason
  attr_reader :en_passant_target, :prev_en_passant_target
  
  def initialize
    @board = create_board
    @castling_allowed = 'KQkq'
    @capture_clock = 0
    @en_passant_target = nil
    @white_piece_locations, @black_piece_locations = create_piece_locations_map
    @prev_white_piece_locations = []
    @prev_black_piece_locations = []

    @pawn_ready_for_promotion = false
    @pawn_to_promote = nil
    @just_promoted_pawn = false
    @invalid_move_reason = 0

    #refactor to game controller
    @game_over_reason = ''
    @turn_to_move = Utils::WHITE
    @fullmove_count = 1
    @previous_halfturn_boards = []
    #TODO: determine if this is needed
    @prev_castling_allowed = []
    @prev_en_passant_target = []
  end

  public

  def get_fen_string
    board_state = BoardState.new(@board, @turn_to_move, @castling_allowed, @en_passant_target, @capture_clock, @fullmove_count)
    FenGenerator.generate_fen_string(board_state)
  end

  def read_fen_string(string)
    board_state = FenReader.read_fen_string(string)
    @board = board_state.board
    @turn_to_move = board_state.active_color
    @fullmove_count = board_state.fullmove_count
    @castling_allowed = board_state.castling
    @en_passant_target = board_state.en_passant_target
    @capture_clock = board_state.halfmove_count 
    @white_piece_locations, @black_piece_locations = create_piece_locations_map
    board_state
  end

  def get_piece(position)
    @board[position.y][position.x]
  end

  def set_position(position, piece)
    @board[position.y][position.x] = piece
  end

#----------------------- board methods end
#----------------------- move methods

  def move_piece(piece_to_move, piece_destination)
    return false if @pawn_ready_for_promotion
    type, color = get_piece_info(piece_to_move)
    return false unless validate_move(piece_to_move, piece_destination)
    do_move(type, color, piece_to_move, piece_destination)
    @just_promoted_pawn = false
    @pawn_ready_for_promotion, @pawn_to_promote = pawn_ready_for_promotion?

    if type == Utils::WHITE_PAWN || type == Utils::BLACK_PAWN
      @capture_clock = 0
    else
      @capture_clock += 1
    end

    true
  end

  def undo_move 
    undo_move_piece
    undo_state
  end

  def invalid_move_reason(piece_to_move, piece_destination)
    type, color = get_piece_info(piece_to_move)
    unless color == @turn_to_move
      if color == Utils::EMPTY
        return 'Must enter a space with a piece!'
      else
        return "Can't move pieces that aren't yours!"
      end
    end

    unless piece_destination.x < Utils::BOARD_WIDTH && piece_destination.x > -1 && piece_destination.y < Utils::BOARD_WIDTH && piece_destination.y > -1
      return 'Must enter a space in the board!'
    end

    valid_moves = get_pseudo_legal_moves(piece_to_move)
    return 'Illegal piece move!' unless valid_moves.include? piece_destination
  end

  def pawn_ready_for_promotion?
    for i in 0..7 do
      black_row_piece = get_piece(Coordinate.new(i, 7))
      white_row_piece = get_piece(Coordinate.new(i, 0))
      if black_row_piece == 1 || white_row_piece == -1 
        col = black_row_piece == 1 ? 7 : 0
        @pawn_ready_for_promotion = true
        @pawn_to_promote = Coordinate.new(i, col)
        return [true, Coordinate.new(i, col)]
      end
    end

    @pawn_ready_for_promotion = false
    @pawn_to_promote = nil
    [false, nil]
  end

  def promote_pawn(piece_to_promote_to)
    return false unless pawn_ready_for_promotion?[0]

    case piece_to_promote_to
    when 'bishop'
      @pawn_ready_for_promotion = false
      promote_piece(3)
    when 'rook'
      @pawn_ready_for_promotion = false
      promote_piece(4)
    when 'knight'
      @pawn_ready_for_promotion = false
      promote_piece(2)
    when 'queen'
      @pawn_ready_for_promotion = false
      promote_piece(5)
    else
      @pawn_ready_for_promotion = true
      return false
    end

    @just_promoted_pawn = true
    true
  end

#----------------------- 
#----------------------- king methods

  def king_in_check?
    king, king_position = get_current_king_info
    color = Utils.get_piece_color(king)
    enemy_locations_map = get_color_pieces(color, true)
    attacking_enemies = enemy_locations_map.select {|location| 
      valid_moves = get_pseudo_legal_moves(Coordinate.new(location[0], location[1]))
      result = false
      valid_moves.each do |move|
        result ||= valid_moves.include? king_position
        break
      end  
      result
    }

    attacking_enemies.length > 0
  end

  def checkmate?
    king_piece = @turn_to_move == Utils::WHITE ? Utils::WHITE_KING : Utils::BLACK_KING
    king_location = get_piece_location(king_piece)

    return false if king_possible_move?(king_location)

    enemy_locations = get_pieces_attacking(king_location)
    return false if enemy_locations.nil?

    allied_defenders = []
    enemy_locations.each do |piece|
      allied_defenders << get_pieces_attacking(piece)
      allied_defenders << get_king_defenders(king_location)
    end

    if allied_defenders.flatten.empty?
      @game_over_reason = "Checkmate"
      true
    end
  end

#----------------------------------
#----------------------------------- game methods
  def draw?
    if @capture_clock >= 50
      @game_over_reason = "50 turn draw"
      return true
    end

    if stalemate?
      @game_over_reason = "Stalemate"
      return true
    end

    false
  end

  def stalemate?
    return false unless king_in_check?
    return false if king_possible_move?   
    !king_in_check? && get_piece_legal_moves(position) == []
  end
#-----------------------------
#------------------------------- piece methods

  def get_piece_legal_moves(position)
    pseudo_legal_moves = get_pseudo_legal_moves(position)
    valid_moves = pseudo_legal_moves.select do |move|
      !self_check?(position, move)
    end

    valid_moves
  end

  def get_piece_info(position)
    piece_type = get_piece(position)
    color = Utils.get_piece_color(piece_type) 
    [piece_type, color]
  end

  def get_color_pieces(color, getOpposing = false)
    {}.replace( (color == Utils::WHITE) ^ getOpposing ? @white_piece_locations : @black_piece_locations)
  end

#-----------------------------
  private
#----------------------- board methods

  def create_board
    board = Array.new(Utils::BOARD_WIDTH) { Array.new(Utils::BOARD_WIDTH) { 0 } }
    # setup black pieces
    board[7] = [Utils::BLACK_ROOK, Utils::BLACK_KNIGHT, Utils::BLACK_BISHOP, Utils::BLACK_QUEEN, Utils::BLACK_KING, Utils::BLACK_BISHOP, Utils::BLACK_KNIGHT, Utils::BLACK_ROOK]
    board[6] = [Utils::BLACK_PAWN, Utils::BLACK_PAWN, Utils::BLACK_PAWN, Utils::BLACK_PAWN, Utils::BLACK_PAWN, Utils::BLACK_PAWN, Utils::BLACK_PAWN, Utils::BLACK_PAWN]
    # setup white pieces
    board[1] = [Utils::WHITE_PAWN, Utils::WHITE_PAWN, Utils::WHITE_PAWN, Utils::WHITE_PAWN, Utils::WHITE_PAWN, Utils::WHITE_PAWN, Utils::WHITE_PAWN, Utils::WHITE_PAWN]
    board[0] = [Utils::WHITE_ROOK, Utils::WHITE_KNIGHT, Utils::WHITE_BISHOP, Utils::WHITE_QUEEN, Utils::WHITE_KING, Utils::WHITE_BISHOP, Utils::WHITE_KNIGHT, Utils::WHITE_ROOK]

    board
  end

  def update_pieces_map(color, map_to_update)
    if color == Utils::WHITE
      @white_piece_locations.replace(map_to_update)
    else
      @black_piece_locations.replace(map_to_update)
    end
  end

  def create_piece_locations_map
    white_piece_locations = {}
    black_piece_locations = {}
    @board.each_with_index do |row, index_row|
      row.each_with_index do |position, index_column|
        unless position == Utils::EMPTY
          location = [index_column, index_row]
          if position > 0
            white_piece_locations[location] = position
          else
            black_piece_locations[location] = position
          end     
        end
      end
    end
    [white_piece_locations, black_piece_locations]
  end

  def update_piece_locations_map
    @prev_black_piece_locations << {}.replace(@black_piece_locations)
    @prev_white_piece_locations << {}.replace(@white_piece_locations)
  end

  def get_piece_location(piece_type)
    result = nil
    color = Utils.get_piece_color(piece_type)
    current_color_locations = get_color_pieces(color)
   
    current_color_locations.each do |current_location, current_piece|
      next if current_piece != piece_type
      coordinate = Coordinate.new(current_location[0],current_location[1])
      result = coordinate 
    end
    result
  end

  def do_move(piece_type, piece_color, piece_to_move, piece_destination)
    @previous_halfturn_boards << Marshal.load(Marshal.dump(@board))
    update_piece_locations_map

    if is_king_castling?(piece_to_move, piece_destination)
      do_castling_move(piece_to_move, piece_destination)
    elsif en_passant_move?(piece_to_move, piece_destination)
      do_en_passant_move(piece_to_move, piece_destination)
    else
      enemy_capture = get_piece(piece_destination) != Utils::EMPTY
      set_position(piece_destination, piece_type)
      set_position(piece_to_move, Utils::EMPTY)

      ally_locations_map = get_color_pieces(piece_color)
      ally_locations_map[[piece_destination.x, piece_destination.y]] = piece_type
      ally_locations_map.delete([piece_to_move.x, piece_to_move.y])

      update_pieces_map(piece_color, ally_locations_map)
      
      if enemy_capture
        enemy_locations_map = get_color_pieces(piece_color, true)
        enemy_locations_map.delete([piece_destination.x, piece_destination.y])

        if piece_color == Utils::WHITE
          @black_piece_locations.replace(enemy_locations_map)
        else
          @white_piece_locations.replace(enemy_locations_map)
        end

      end

    end

    update_castling(piece_type, piece_to_move.x) 
    update_en_passant(piece_type, piece_to_move, piece_destination)
    update_move_counters
    true
  end

  def validate_move(position, destination)
    type, color = get_piece_info(position)
    return false unless color == @turn_to_move
    valid_moves = get_pseudo_legal_moves(position)
    return false unless valid_moves.include? destination
    return false  if self_check?(position, destination)
    return false if is_king_castling?(position, destination) && !is_castling_move_safe?(position, destination)
    true
  end

  def undo_move_piece
    return false if @previous_halfturn_boards.empty?
    @board = @previous_halfturn_boards.pop
    undo_update_move_counters
    true
  end

  def undo_state
    @pawn_ready_for_promotion, @pawn_to_promote = pawn_ready_for_promotion?
    @castling_allowed = @prev_castling_allowed.empty? ? @castling_allowed : @prev_castling_allowed.pop
    @en_passant_target = @prev_en_passant_target.empty? ? @en_passant_target : @prev_en_passant_target.pop
    @white_piece_locations = @prev_white_piece_locations.empty? ? @white_piece_locations : @prev_white_piece_locations.pop
    @black_piece_locations = @prev_black_piece_locations.empty? ? @black_piece_locations : @prev_black_piece_locations.pop
    @capture_clock -= 1 if @capture_clock != 0
  end

  def update_move_counters
    if @turn_to_move == Utils::WHITE
      @turn_to_move = Utils::BLACK
    else
      @turn_to_move = Utils::WHITE
      @fullmove_count += 1
    end
  end

  def undo_update_move_counters
    if @turn_to_move == Utils::WHITE
      @turn_to_move = Utils::BLACK
      @fullmove_count -= 1
    else
      @turn_to_move = Utils::WHITE
    end
  end

  def update_castling(piece_type, move_x)
    @prev_castling_allowed << @castling_allowed
    if piece_type == Utils::WHITE_KING
      @castling_allowed = @castling_allowed.delete('K', 'Q')
    elsif piece_type == Utils::BLACK_KING
      @castling_allowed = @castling_allowed.delete('k', 'q')
    elsif piece_type == Utils::WHITE_ROOK && move_x == 2
      @castling_allowed = @castling_allowed.delete('Q')
    elsif piece_type == Utils::WHITE_ROOK && move_x == 6
      @castling_allowed = @castling_allowed.delete('K')
    elsif piece_type == Utils::BLACK_ROOK && move_x == 2
      @castling_allowed = @castling_allowed.delete('q')
    elsif piece_type == Utils::BLACK_ROOK && move_x == 6
      @castling_allowed = @castling_allowed.delete('k')
    end

    @castling_allowed = '-' if @castling_allowed.empty?
  end

#------------------------------------
#------------------------------------ king methods
  
  def king_possible_move?(king_location)
    return true unless king_in_check?

    # do any of the kings moves remove check?
    valid_moves = get_pseudo_legal_moves(king_location)
    valid_moves.each do |move|
      next if self_check?(king_location, move)
      return true
    end

    false
  end

  def get_king_defenders(king_location)
    if Utils.get_piece_color(king_location) == Utils::WHITE
      ally_locations_map.replace(@white_piece_locations)
    else
      ally_locations_map.replace(@black_piece_locations)
    end

    defenders = ally_locations_map.select do |location|
      coordinate = Coordinate.new(location[0], location[1])
      valid_moves = get_pseudo_legal_moves(coordinate)
      result = false
      valid_moves.each do |move|
        next if self_check?(coordinate, move)
        result = true
        break
      end

      result
    end

    defenders
  end
 
  def get_pieces_attacking(piece_location)
    type, color = get_piece_info(piece_location)
    enemy_locations_map = get_color_pieces(color, true)

    attacking_locations = enemy_locations_map.select {|location| 
      valid_moves = get_pseudo_legal_moves(Coordinate.new(location[1],location[0]))
      result = false
      valid_moves.each do |move|
        next if self_check?(piece_location, move)
        next unless move == piece_location
        result = true
        break
      end  
  
      result
    }
    attacking_locations
  end

  def get_current_king_info
    king = @turn_to_move == Utils::WHITE ? Utils::WHITE_KING : Utils::BLACK_KING
   
    king_position = get_piece_location(king)
    [king, king_position]
  end

#------------------------------------
#------------------------------------ move methods
  
  def get_pseudo_legal_moves(piece)
    move_generator = PieceValidMoves.new(@board)
    move_generator.generate_pseudo_legal_moves(piece, @en_passant_target, @castling_allowed)
  end

#------------------------------------

  def get_legal_moves(piece_color)
    ally_locations_map = get_color_pieces(piece_color)

    legal_moves = []
    ally_locations_map.select do |location|
      coordinate = Coordinate.new(location[0], location[1])
      valid_moves = get_pseudo_legal_moves(coordinate)
      valid_moves.each_with_index do |move, _index|
        next if self_check?(coordinate, move)
        legal_moves << move 
        break
      end
    end
    return [false, nil] if legal_moves.empty?
    [true, legal_moves]
  end

  def self_check?(piece_to_move, piece_destination)
    piece_type, piece_color = get_piece_info(piece_to_move)
    king, king_position = get_current_king_info # this must be done before the move is made
    move_result = do_move(piece_type, piece_color, piece_to_move, piece_destination) 
    # king position stores the position of the king before the move is made
    king_position = piece_destination if piece_type == king 
    is_check = piece_under_attack?(king, king_position)
    undo_move 
    move_result && is_check
  end

  def piece_under_attack?(piece_type, piece_location) 
    color = Utils.get_piece_color(piece_type)
    enemy_locations_map = get_color_pieces(color, true)

    targeting_enemies = enemy_locations_map.select {|location| 
      valid_moves = get_pseudo_legal_moves(Coordinate.new(location[0],location[1]))
      result = false
      valid_moves.each do |move|
        result ||= valid_moves.include? piece_location
        break
      end  
  
      result
    }
   
    targeting_enemies.length > 0
  end

#------------------------ castling methods

  def is_king_castling?(piece_to_move, piece_destination)
    type, color = get_piece_info(piece_to_move)
    difference = piece_destination.x - piece_to_move.x
    valid_difference = difference == 2 || difference == -2
    valid_x = piece_to_move.x == 4 && (piece_destination.x == 6 || piece_destination.x == 2)
    valid_y = piece_to_move.y == 7 || piece_to_move.y == 0
    valid_destination_y  = piece_destination.y == 7 || piece_destination.y == 0
    valid_type = type == Utils::WHITE_KING || type == Utils::BLACK_KING
    valid_difference && valid_x && valid_y && valid_destination_y && valid_type
  end

  def is_castling_move_safe?(piece_to_move, piece_destination)
    type, color = get_piece_info(piece_to_move)
    is_white_king = type == Utils::WHITE_KING
    is_black_king = type == Utils::BLACK_KING
    return false if (!is_white_king && !is_black_king)

    return false if self_check?(piece_to_move, Coordinate.new(piece_destination.x, piece_destination.y))
    
    king_location = [piece_to_move.x, piece_to_move.y]
    left_destination = [piece_destination.x - 1, piece_destination.y]
    right_destination = [piece_destination.x + 1, piece_destination.y]

    ally_locations_map = get_color_pieces(color)

    #remove the king to test moves that wont happen normally
    ally_locations_map.delete(king_location)

    if piece_destination.x < piece_to_move.x
      # left castling
      is_left_safe = !self_check?(piece_to_move, Coordinate.new(piece_destination.x + 1, piece_destination.y))
    else
      # right castling
      is_right_safe = !self_check?(piece_to_move, Coordinate.new(piece_destination.x - 1, piece_destination.y))
    end

    #put king back in
    ally_locations_map[king_location] = type
    update_pieces_map(color, ally_locations_map)

    is_king_in_check = king_in_check?
    return is_right_safe if !is_king_in_check && piece_destination.x == 6 && is_white_king   
    return is_left_safe  if !is_king_in_check && piece_destination.x == 2 && is_white_king 
    return is_right_safe if !is_king_in_check && piece_destination.x == 6 && is_black_king
    return is_left_safe  if !is_king_in_check && piece_destination.x == 2 && is_black_king      
    return false
  end

  def do_castling_move(location, destination)
    type, color = get_piece_info(location)
    # move king
    set_position(destination, type)
    set_position(location, Utils::EMPTY)
    
    ally_locations_map = get_color_pieces(color)
    ally_locations_map[[destination.x, destination.y]] = type
    ally_locations_map.delete([location.x, location.y])

    # move rook
    if destination.x == 6
      set_position(Coordinate.new(destination.x - 1, destination.y), 4 * color)
      set_position(Coordinate.new(destination.x + 1, destination.y), Utils::EMPTY)
      ally_locations_map[[destination.x - 1, destination.y]] = 4 * color
      ally_locations_map.delete([destination.x + 1, destination.y])
    elsif destination.x == 2
      set_position(Coordinate.new(destination.x + 1, destination.y), 4 * color)
      set_position(Coordinate.new(destination.x - 2, destination.y), Utils::EMPTY)
      ally_locations_map[[destination.x + 1, destination.y]] = 4 * color
      ally_locations_map.delete([destination.x - 2, destination.y])
    end

    update_pieces_map(color, ally_locations_map)
  end
#------------------------------------------------------
#------------------------------------------------------en passant or piece
  def promote_piece(piece_to_promote_to)
    type, color = get_piece_info(@pawn_to_promote)
    set_position(@pawn_to_promote, piece_to_promote_to * color)

    ally_locations_map = get_color_pieces(color)
    ally_locations_map[[@pawn_to_promote.x, @pawn_to_promote.y]] = piece_to_promote_to * color
    update_pieces_map(color, ally_locations_map)

    @pawn_to_promote = nil
    true
  end

  def update_en_passant(piece_type, piece_to_move, piece_destination)
    @prev_en_passant_target << @en_passant_target
    @en_passant_target = nil

    if ( piece_type == Utils::WHITE_PAWN || piece_type == Utils::BLACK_PAWN) && piece_destination.y == piece_to_move.y + (2 * piece_type)
      @en_passant_target = Coordinate.new(piece_destination.x, piece_destination.y - piece_type)
    end 
  end

  def en_passant_move?(piece_to_move, piece_destination)
    return false if @en_passant_target.nil?
    type, color = get_piece_info(piece_to_move)
    return false unless type == Utils::WHITE_PAWN || type == Utils::BLACK_PAWN
    #if @en_passant_target.y == 2 then the target is a white pawn
    #if @en_passant_target.y == 5 then the target is a black pawn
    piece_at_destination = get_piece(piece_destination)
    if piece_at_destination == Utils::EMPTY && piece_destination == @en_passant_target
      return true if type == Utils::BLACK_PAWN && @en_passant_target.y == 2 
      return true if type == Utils::WHITE_PAWN && @en_passant_target.y == 5
    end

    false
  end

  def do_en_passant_move(piece_to_move, piece_destination)
    type, color = get_piece_info(piece_to_move)
    set_position(piece_destination, type)
    set_position(piece_to_move, Utils::EMPTY)
    set_position(Coordinate.new(piece_destination.x, piece_destination.y - type), Utils::EMPTY)
 
    ally_locations_map = get_color_pieces(color)
    enemy_locations_map = get_color_pieces(color, true)
    ally_locations_map[[piece_destination.x, piece_destination.y]] = type
    ally_locations_map.delete([piece_to_move.x, piece_to_move.y])
    enemy_locations_map.delete([piece_destination.x, piece_destination.y - type])
    
    if color == Utils::WHITE 
      @white_piece_locations.replace(ally_locations_map)
      @black_piece_locations.replace(enemy_locations_map)
    else
      @black_piece_locations.replace(ally_locations_map)
      @white_piece_locations.replace(enemy_locations_map)
    end
  
  end

#------------------------------------------------------
end