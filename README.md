# Red Queen

<div align="center">
![alt text](./img/red-queen.png)
</div>

## Description

Red Queen is a chess engine written in Ruby. It can play awkward 2 player games in terminal or can be used to evaluate chess positions.
This project was a tool for me to practice Ruby concepts, Test Driven Development and Object Oriented Programming.

### Features

- Command Line Interface
- Perft Test Suite
- Fen String Board Representation
- ASCII Board Display
- Loading and Saving Games
- Move Validation
- Check and Checkmate
- Castling
- En Passant
- Pawn Promotion

### Future Work

- Refactor: Board class needs to be refactored into several classes because it has more than one responsibility.
- AI Player: A computer controlled player would allow the engine to be useful for a single player.
- Improve Performance: The speed of the engine is good enough for human players but needs to be improved for an AI to be added.
- PGN Records: Saving the games in PGN format would allow save files to be imported or exported to other chess engines. Currently only board states can be used as such.

## Built With

- [Ruby](https://www.ruby-lang.org/en/) - Version 2.5.5
- [RSpec](https://rspec.info/) - Test Suite. Version 3.8

Has not been tested on older versions.

### Prerequisites

Ruby can be installed based on your OS [here](https://www.ruby-lang.org/en/downloads/)

Rspec can be installed as a gem:

```
gem install rspec
```

### Installing

To install just pull the project folder and from command line use:

```
ruby lib/chess.rb
```

This will run the game.

## Running the tests

Tests were made with Rspec:

```
rspec
```

or for more verbose results

```
rspec -f d
```

### Validation Tests

Several tests in the suite are for testing the correctness of the board and take much longer than the rest. These tests have been disabled by default.

## Authors

- **Alain Suarez** - _Initial work_

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
